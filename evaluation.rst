Travail pratique 2
==================

Étudiants
---------

- Pierre-Alexandre Lassonde
- Julien Perron

Évaluation
----------

+-------------------------+----------------------------+-----------+-----------+
| Critère                 | Sous-critère               | Note      | Sur       |
+=========================+============================+===========+===========+
|                         | Rues et terrains           | 9         | 10        |
|                         +----------------------------+-----------+-----------+
|                         | Immeubles commerciaux      | 10        | 10        |
|                         +----------------------------+-----------+-----------+
|                         | Immeubles résidentiels     | 15        | 15        |
| Fonctionnabilité        +----------------------------+-----------+-----------+
|                         | Lieux publics              | 15        | 15        |
|                         +----------------------------+-----------+-----------+
|                         | Animation                  | 9         | 10        |
|                         +----------------------------+-----------+-----------+
|                         | Détection de collision     | 20        | 20        |
|                         +----------------------------+-----------+-----------+
|                         | Paramétrisation            | 5         | 5         |
+-------------------------+----------------------------+-----------+-----------+
|                         | Rues et lieux publics      | 10        | 10        |
|                         +----------------------------+-----------+-----------+
| Qualité graphique       | Immeubles                  | 10        | 10        |
|                         +----------------------------+-----------+-----------+
|                         | Animation                  | 10        | 10        |
+-------------------------+----------------------------+-----------+-----------+
|                         | Fichier README             | 10        | 10        |
|                         +----------------------------+-----------+-----------+
| Qualité de la remise    | Style de programmation     | 15        | 15        |
|                         +----------------------------+-----------+-----------+
|                         | Utilisation de Git         | 10        | 10        |
+-------------------------+----------------------------+-----------+-----------+
| Total                                                | 148       | 150       |
+-------------------------+----------------------------+-----------+-----------+

Commentaires
------------

- Beaux modèles et belle qualité graphique.
- La texture de la rue manque de précision et est floue.
- Déplacements fluides, mais un peu trop contraints (que des virages à 90
  degrés).
- Bonne décomposition modulaire du projet.
- Bonne documentation.
- README correct.
- Utilisation de Git un peu minimaliste (messages de commits pas très précis).
